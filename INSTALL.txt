INSTALLATION
=============================
1. Place in the modules directory
2. Install module from the administration menu
3. Update the top line in payment_payflowpro.module that says:
define('PAYFLOWPRO_PWD', 'put-your-password-here');
replace 'put-your-password-here' with the password to your payflow pro API.
READ ME
====================

This module was created to interface with the 
PayFlow Pro payment gateway (formerly Verisign but 
now PayPal). 

Security
---------------
It is recommended that you create a 
seperate username and password for Drupal that has 
very limited access to the API. It is best to 
restrict via IP address and privileges. See your 
PayFlow Pro documentation for more information on this.